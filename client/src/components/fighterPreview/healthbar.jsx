import React from "react";

export default function Healthbar({ healthPercentage }) {
    return (
        <div className='fighter-preview___healthbar'>
            <div className='fighter-preview___healthbar-progress'
            style={{width: `${healthPercentage}%`}}></div>
        </div>
    )
}