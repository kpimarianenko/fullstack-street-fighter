import React from "react";
import './fighterPreview.css';
import Healthbar from './healthbar';
import Fighter from './fighter';

export default function FighterPreview({ name, healthPercentage, pos }) {
    return (
        <div className="fighter-preview">
            <div className="fighter-preview___info">
                <div className="fighter-preview___name">
                    {name}
                </div>
                <Healthbar healthPercentage={healthPercentage} />
            </div>
            <Fighter pos={pos} />
        </div>
    )
}