import React from "react";

export default function Fighter({ pos }) {
    return (
        <img className={`fighter-preview___${pos}-fighter`}
            src={`/images/fighter.gif`}
            alt={`${pos} fighter`} />
    )
}