import React from "react";
import './modal.css';

export default function Modal({ show, fighter, onClose }) {
    return show ? (
        <div className="modal-layer">
            <div className="modal-root">
                <div className="modal-header">
                    <span>{`${fighter.name} won!`}</span>
                    <div onClick={onClose} className="close-btn">×</div>
                </div>
                <div className="modal-body">
                    <p>Congratulations! Close this window to fight again!</p>
                    <img src="/images/ko.png" alt="K.O." />
                </div>
            </div>
        </div>
    ) : null;
}