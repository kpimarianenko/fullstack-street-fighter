import { unsetLoginSession } from "../../services/authService";
import React from 'react';
import { Button } from '@material-ui/core';
import './signOut.css';

export default function SignOut({ isSignedIn, onSignOut}) {
    const signOut = () => {
        unsetLoginSession();
        onSignOut();
    }

    if(isSignedIn) {
        return (
            <Button onClick={signOut} id="sign-out" variant="contained" color="primary">Sign Out</Button>
        )
    }

    return null;
}