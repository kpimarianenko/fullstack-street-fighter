import React, { useEffect, useState } from "react";
import Modal from '../modal';
import { updateFight } from '../../services/domainRequest/fightRequest';
import { getRandomFloat } from '../../services/randomHelper';
import { controls } from '../../constants/controls';
import FighterPreview from "../fighterPreview";
import { pressedKeys, PressedKeysService } from "../../services/pressedKeysService";
import './arena.css';

export default function Arena({ fighter1, fighter2, onModalClose, fightID }) {
    const critHitCoolDown = 10000;
    const [log, setLog] = useState([]);
    const [winner, setWinner] = useState({});
    const [showModal, setShowModal] = useState(false);
    const [f1State, setF1State] = useState(getStartFighterState(fighter1, 'left'));
    const [f2State, setF2State] = useState(getStartFighterState(fighter2, 'right'));

    const calculatePercentage = (fighter, fighterState) => {
        return fighterState.health / fighter.health * 100;
    }

    const modifyFighterState = (state, setState, updatedState) => {
        const keys = Object.keys(updatedState);
        const newState = {};
        Object.assign(newState, state)
        keys.forEach(key => {
            newState[key] = updatedState[key];
        });
        setState(newState);
    }

    const attack = (atc, def, atcState, defState, setDefState) => {
        if (isFighterBlocked(defState) || isFighterBlocked(atcState)) return;
        const dmg = getDamage(atc, def);
        pushToLog(`${atc.name} deal ${dmg} damage`);
        const newHealth = Math.max(defState.health - dmg, 0);
        if (newHealth <= 0) {
            setWinner(atc);
            setShowModal(true);
            pushToLog(`${atc.name} win!`);
        }
        const newState = { health: newHealth }
        modifyFighterState(defState, setDefState, newState);
    }

    const critAttack = (atc, atcState, defState, setAtcState, setDefState) => {
        if (!atcState.canCrit || isFighterBlocked(atcState)) return;
        modifyFighterState(atcState, setAtcState, { canCrit: false });
        setTimeout(() => modifyFighterState(atcState, setAtcState, { canCrit: true }), critHitCoolDown);
        const dmg = atc.power * 2;
        pushToLog(`${atc.name} deal ${dmg} damage`);
        const newHealth = Math.max(defState.health - dmg, 0);
        if (newHealth <= 0) {
            setWinner(atc);
            setShowModal(true);
            pushToLog(`${atc.name} win!`);
        }
        const newState = { health: newHealth }
        modifyFighterState(defState, setDefState, newState);
    }

    const pushToLog = (str) => {
        const newLog = [...log];
        newLog.push(str);
        setLog(newLog);
        updateFight(fightID, { log: newLog });
    }

    const isFighterBlocked = (fighter) => {
        return pressedKeys.has(fighter.position === 'left' ? controls.PlayerOneBlock : controls.PlayerTwoBlock);
    }

    function getDamage(attacker, defender) {
        const damage = getHitPower(attacker) - getBlockPower(defender);
        return Math.max(damage, 0);
    }

    function getHitPower(fighter) {
        const { power } = fighter;
        const criticalHitChance = getRandomFloat(1, 2);
        const dmg = power * criticalHitChance;
        return dmg;
    }

    function getBlockPower(fighter) {
        const { defense } = fighter;
        const dodgeChance = getRandomFloat(1, 2);
        const power = defense * dodgeChance;
        return power;
    }

    useEffect(() => {
        new PressedKeysService({
            [controls.PlayerOneAttack]: () => attack(fighter1, fighter2, f1State, f2State, setF2State),
            [controls.PlayerTwoAttack]: () => attack(fighter2, fighter1, f2State, f1State, setF1State),
            [controls.PlayerOneCriticalHitCombination]: () => {
                critAttack(fighter1, f1State, f2State, setF1State, setF2State);
            },
            [controls.PlayerTwoCriticalHitCombination]: () => {
                critAttack(fighter2, f2State, f1State, setF2State, setF1State);
            }
        });
    })

    return (
        <div className="arena">
            <FighterPreview name={fighter1.name}
                healthPercentage={calculatePercentage(fighter1, f1State)}
                pos={f1State.position} />
            <FighterPreview name={fighter2.name}
                healthPercentage={calculatePercentage(fighter2, f2State)}
                pos={f2State.position} />
            <Modal fighter={winner} show={showModal} onClose={onModalClose} />
        </div>
    )
}

function getStartFighterState(fighter, position) {
    const { health } = fighter;
    return {
        health,
        position,
        canCrit: true,
    }
}