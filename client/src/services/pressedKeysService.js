export const pressedKeys = new Set();

export class PressedKeysService {
  constructor(rules) {
    this.rules = rules;

    document.body.onkeydown = (ev) => {
      const { code } = ev;
      if (!pressedKeys.has(code)) {
        const prevPressedKeys = new Set(pressedKeys);
        pressedKeys.add(code);
        this.checkRules(prevPressedKeys);
      }
    }

    document.body.onkeyup = (ev) => {
      const { code } = ev;
      pressedKeys.delete(code);
    }
  }

  checkRules(prevPressedKeys) {
    for (const [key, value] of Object.entries(this.rules)) {
      const combination = key.split(',');

      const isCurKeysHasCombination = combination.every((el) => {
        return pressedKeys.has(el);
      })

      const isPrevKeysHasCombination = combination.every((el) => {
        return prevPressedKeys.has(el);
      })

      if (isCurKeysHasCombination && !isPrevKeysHasCombination) {
        value();
      }
    }
  }
}