const { bodyHasAllData,
    allDataIsValid,
    getEntityFromModel,
    bodyDataSatisfiesModel } = require('../helpers/validateHelper');
const statusCodes = require('../constants/statusCodes');
const HTTPError = require('../helpers/httperror');

const newCreateValidMiddleware = (model, ignoreKeys, validationRules, entityName) => {
    return (req, res, next) => {
        try {
            const { body } = req;

            const isBodyHasExtraData = !bodyDataSatisfiesModel(body, model, ignoreKeys);
            const isBodyHasAllData = bodyHasAllData(body, model, ignoreKeys);
            const isAllDataCorrect = allDataIsValid(body, validationRules);

            if (!isBodyHasAllData || !isAllDataCorrect || isBodyHasExtraData) {
                throw new HTTPError(`${entityName} entity to create is not valid`, statusCodes.badRequest);
            }

            res.data = getEntityFromModel(body, model, ignoreKeys);
        }
        catch (err) {
            res.err = err;
        }
        finally {
            next();
        }
    }
}

const newUpdateValidMiddleware = (model, ignoreKeys, validationRules, entityName) => {
    return (req, res, next) => {
        try {
            const { body } = req;

            const isBodyHasExtraData = !bodyDataSatisfiesModel(body, model, ignoreKeys);
            const isAllDataCorrect = allDataIsValid(body, validationRules);

            if (!isAllDataCorrect || isBodyHasExtraData) {
                throw new HTTPError(`${entityName} entity to update is not valid`, statusCodes.badRequest);
            }

            res.data = getEntityFromModel(body, model, ignoreKeys);
        }
        catch (err) {
            res.err = err;
        }
        finally {
            next();
        }
    }
}

exports.newCreateValidMiddleware = newCreateValidMiddleware;
exports.newUpdateValidMiddleware = newUpdateValidMiddleware;