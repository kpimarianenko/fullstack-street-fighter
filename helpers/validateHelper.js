const { uuid4 } = require('../constants/regexp');
const regExps = require('../constants/regexp');

const validator = {
    minLen: (value, ruleValue) => value.length >= ruleValue,
    maxLen: (value, ruleValue) => value.length <= ruleValue,
    min: (value, ruleValue) => value >= ruleValue,
    max: (value, ruleValue) => value <= ruleValue,
    email: (value) => value.match(regExps.email),
    phoneNumber: (value) => value.match(regExps.phoneNumber),
    uuid4: (value) => value.match(regExps.uuid4),
    array: (value) => Array.isArray(value)
}

const bodyHasAllData = (body, model, ignoredKeys) => {
    const modelKeys = Object.keys(model);
    
    return modelKeys.every(key => {
        return ignoredKeys.includes(key) ? true : !!body[key];
    });
}

const bodyDataSatisfiesModel= (body, model, ignoredKeys) => {
    const modelKeys = Object.keys(model);
    const bodyKeys = Object.keys(body);
    
    return bodyKeys.every(key => {
        return ignoredKeys.includes(key) ? false : modelKeys.includes(key);
    });
}

const allDataIsValid = (body, rules) => {
    return Object.entries(rules).every(([key, rule]) => {
        const value = body[key];
        if (!value) return true;
        return validateField(value, rule);
    });
}

const validateField = (value, rule) => {
    return Object.entries(rule).every(([key, ruleValue]) => {
        return validator[key](value, ruleValue);
    });
}

const getEntityFromModel = (body, model, ignoredKeys = []) => {
    const entity = {};
    Object.keys(model).forEach(key => {
        const value = body[key];
        if (!ignoredKeys.includes(key) && value) {
            if (typeof model[key] === 'number') {
                entity[key] = parseFloat(body[key]);
            } else {
                entity[key] = body[key];
            }
        }
    });
    return entity;
}

exports.bodyHasAllData = bodyHasAllData;
exports.allDataIsValid = allDataIsValid;
exports.getEntityFromModel = getEntityFromModel;
exports.bodyDataSatisfiesModel = bodyDataSatisfiesModel;
