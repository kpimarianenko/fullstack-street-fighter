const { user } = require('../models/user');
const { newCreateValidMiddleware, newUpdateValidMiddleware } = require('../helpers/validationMiddlewareCreator');

const entityName = 'User';
const ignoreKeys = ["id"];
const validationRules = {
    password: {
        minLen: 3
    },
    phoneNumber: {
        phoneNumber: true
    },
    email: {
        email: true
    }
};

const createUserValid = newCreateValidMiddleware(user, ignoreKeys, validationRules, entityName);
const updateUserValid = newUpdateValidMiddleware(user, ignoreKeys, validationRules, entityName);

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;