const { fight } = require('../models/fight');
const { newCreateValidMiddleware, newUpdateValidMiddleware } = require('../helpers/validationMiddlewareCreator');

const entityName = 'Fight';
const createIgnoreKeys = ["id", "log"];
const updateIgnoreKeys = ["id"];
const validationRules = {
    fighter1: {
        uuid4: true
    },
    fighter2: {
        uuid4: true
    },
    log: {
        array: true
    }
};

const createFightValid = newCreateValidMiddleware(fight, createIgnoreKeys, validationRules, entityName);
const updateFightValid = newUpdateValidMiddleware(fight, updateIgnoreKeys, validationRules, entityName);

exports.createFightValid = createFightValid;
exports.updateFightValid = updateFightValid;