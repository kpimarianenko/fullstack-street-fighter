const { fighter } = require('../models/fighter');
const { newCreateValidMiddleware, newUpdateValidMiddleware } = require('../helpers/validationMiddlewareCreator');

const entityName = 'Fighter';
const ignoreKeys = ["id", "health"];
const validationRules = {
    defense: {
        min: 1,
        max: 10
    },
    power: {
        min: 0,
        max: 100,
    }
};

const createFighterValid = newCreateValidMiddleware(fighter, ignoreKeys, validationRules, entityName);
const updateFighterValid = newUpdateValidMiddleware(fighter, ignoreKeys, validationRules, entityName);

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;