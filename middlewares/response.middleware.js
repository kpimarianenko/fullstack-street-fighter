const statusCodes = require('../constants/statusCodes');

const responseMiddleware = (req, res, next) => {
    if (res.err) res.status(res.err.status || statusCodes.serverError).json({ 
        error: true,
        message: res.err.message
    });
    else res.json(res.data);
    next();
}

exports.responseMiddleware = responseMiddleware;