const Controller = require('./controller');
const AuthService = require('../services/authService');

class AuthController extends Controller {
    loginController() {
        return this.createController((req, res, next) => {
            const credentials = {
                email: req.body.email,
                password: req.body.password
            }
            const data = AuthService.login(credentials);
            res.data = data;
        })
    }
}

module.exports = new AuthController();