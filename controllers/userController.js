const Controller = require('./controller');
const UserService = require('../services/userService');

class UserController extends Controller {
    getAllController() {
        return this.createController((req, res, next) => {
            const users = UserService.getAll();
            res.data = users;
        })
    }

    getByIdController() {
        return this.createController((req, res, next) => {
            const user = UserService.getById(req.params.id);
            res.data = user;
        })
    }

    postController() {
        return this.createController((req, res, next) => {
            if (res.err) next();
            const user = UserService.create(res.data);
            res.data = user;
        })
    }

    updateController() {
        return this.createController((req, res, next) => {
            if (res.err) next();
            const user = UserService.updateById(req.params.id, req.body);
            res.data = user;
        })
    }

    deleteController() {
        return this.createController((req, res, next) => {
            const user = UserService.deleteById(req.params.id);
            res.data = user;
        })
    }
}

module.exports = new UserController();