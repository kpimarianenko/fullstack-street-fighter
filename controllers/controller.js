module.exports = class Controller {
    createController(callback) {
        return (req, res, next) => {
            try {
                callback(req, res, next);
            } catch (err) {
                res.err = err;
            } finally {
                next();
            }
        }
    }
}