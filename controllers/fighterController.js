const Controller = require('./controller');
const FighterService = require('../services/fighterService');

class FighterController extends Controller {
    getAllController() {
        return this.createController((req, res, next) => {
            const fighters = FighterService.getAll();
            res.data = fighters;
        })
    }

    getByIdController() {
        return this.createController((req, res, next) => {
            const fighter = FighterService.getById(req.params.id);
            res.data = fighter;
        })
    }

    postController() {
        return this.createController((req, res, next) => {
            if (res.err) next();
            const fighter = FighterService.create(res.data);
            res.data = fighter;
        })
    }

    updateController() {
        return this.createController((req, res, next) => {
            if (res.err) next();
            const fighter = FighterService.updateById(req.params.id, req.body);
            res.data = fighter;
        })
    }

    deleteController() {
        return this.createController((req, res, next) => {
            const fighter = FighterService.deleteById(req.params.id);
            res.data = fighter;
        })
    }
}

module.exports = new FighterController();