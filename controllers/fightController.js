const Controller = require('./controller');
const FightService = require('../services/fightService');

class AuthController extends Controller {
    getAllController() {
        return this.createController((req, res, next) => {
            const fights = FightService.getAll();
            res.data = fights;
        })
    }

    getByIdController() {
        return this.createController((req, res, next) => {
            const fight = FightService.getById(req.params.id);
            res.data = fight;
        })
    }

    postController() {
        return this.createController((req, res, next) => {
            if (res.err) next();
            const fight = FightService.create(res.data);
            res.data = fight;
        })
    }

    updateController() {
        return this.createController((req, res, next) => {
            if (res.err) next();
            const fight = FightService.updateById(req.params.id, req.body);
            res.data = fight;
        })
    }

    deleteController() {
        return this.createController((req, res, next) => {
            const fight = FightService.deleteById(req.params.id);
            res.data = fight;
        })
    }
}

module.exports = new AuthController();