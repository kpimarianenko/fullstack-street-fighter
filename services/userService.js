const { UserRepository } = require('../repositories/userRepository');
const statusCodes = require('../constants/statusCodes');
const HTTPError = require('../helpers/httperror');

class UserService {

    getAll() {
        const users = UserRepository.getAll();
        if(!users || users.length === 0) {
            throw new HTTPError('Users not found', statusCodes.notFound);
        }
        return users;
    }

    getById(id) {
        const user = UserRepository.getOne({ id });
        if (!user) {
            throw new HTTPError('User not found', statusCodes.notFound);
        }
        return user;
    }

    create(data) {
        const userEmail = this.search({ email: data.email });
        if (userEmail) {
            throw new HTTPError('User with this email already registered', statusCodes.badRequest);
        }
        const userPhoneNumber = this.search({ phoneNumber: data.phoneNumber });
        if (userPhoneNumber) {
            throw new HTTPError('User with this phone number already registered', statusCodes.badRequest);
        }
        return UserRepository.create(data);
    }

    updateById(id, data) {
        const user = UserRepository.getOne({ id });
        if (!user) {
            throw new HTTPError('User to update is not found', statusCodes.notFound);
        }
        Object.keys(user).forEach((key) => {
            if(data[key]) {
                user[key] = data[key];
            }
        })
        const updatedUser = UserRepository.update(id, user);
        return updatedUser;
    }

    deleteById(id) {
        const user = UserRepository.delete(id);
        if (user.length === 0) {
            throw new HTTPError('User to delete is not found', statusCodes.notFound);
        }
        return user[0];
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
}

module.exports = new UserService();