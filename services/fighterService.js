const { FighterRepository } = require('../repositories/fighterRepository');
const { fighter } = require('../models/fighter');
const statusCodes = require('../constants/statusCodes');
const HTTPError = require('../helpers/httperror');

class FighterService {
    
    getAll() {
        const fighters = FighterRepository.getAll();
        if(!fighters || fighters.length === 0) {
            throw new HTTPError('Fighters not found', statusCodes.notFound);
        }
        return fighters;
    }

    getById(id) {
        const fighter = FighterRepository.getOne({ id });
        if (!fighter) {
            throw new HTTPError('Fighter not found', statusCodes.notFound);
        }
        return fighter;
    }

    create(data) {
        data.health = fighter.health;
        const item = this.search({ name: data.name });
        if (item) {
            throw new HTTPError('Fighter with this name already exists', statusCodes.badRequest);
        }
        return FighterRepository.create(data);;
    }

    updateById(id, data) {
        const fighter = FighterRepository.getOne({ id });
        if (!fighter) {
            throw new HTTPError('Fighter to update is not found', statusCodes.notFound);
        }
        Object.keys(fighter).forEach((key) => {
            if(data[key]) {
                fighter[key] = data[key];
            }
        })
        const updatedFighter = FighterRepository.update(id, fighter);
        return updatedFighter;
    }

    deleteById(id) {
        const fighter = FighterRepository.delete(id);
        if (fighter.length === 0) {
            throw new HTTPError('Fighter to delete is not found', statusCodes.notFound);
        }
        return fighter[0];
    }

    search(search) {
        const item = FighterRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
}

module.exports = new FighterService();