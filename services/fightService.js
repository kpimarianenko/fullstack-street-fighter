const { FightRepository } = require('../repositories/fightRepository');
const FighterService = require('./fighterService');
const HTTPError = require('../helpers/httperror');
const statusCodes = require('../constants/statusCodes');

class FightService {

    getAll() {
        const fights = FightRepository.getAll();
        if(!fights || fights.length === 0) {
            throw new HTTPError('Fights not found', statusCodes.notFound);
        }
        return fights;
    }

    getById(id) {
        const fight = FightRepository.getOne({ id });
        if (!fight) {
            throw new HTTPError('Fight not found', statusCodes.notFound);
        }
        return fight;
    }

    create(data) {
        const fighter1 = FighterService.search({ id: data.fighter1 });
        const fighter2 = FighterService.search({ id: data.fighter2 });
        if (!fighter1 || !fighter2) {
            throw new HTTPError('Fighters to create fight is not found', statusCodes.notFound);
        }
        data.log = [];
        return FightRepository.create(data);
    }

    updateById(id, data) {
        const fight = FightRepository.getOne({ id });
        if (!fight) {
            throw new HTTPError('Fight to update is not found', statusCodes.notFound);
        }
        const { fighter1, fighter2 } = data;
        const isFighter1Invalid = fighter1 && !FighterService.search({ id: fighter1 });
        const isFighter2Invalid = fighter2 && !FighterService.search({ id: fighter2 });
        if (isFighter1Invalid || isFighter2Invalid) {
            throw new HTTPError('Fighters to update fight is not found', statusCodes.notFound);
        }
        Object.keys(fight).forEach((key) => {
            if(data[key]) {
                fight[key] = data[key];
            }
        })
        const updatedFight = FightRepository.update(id, fight);
        return updatedFight;
    }

    deleteById(id) {
        const fight = FightRepository.delete(id);
        if (fight.length === 0) {
            throw new HTTPError('Fight to delete is not found', statusCodes.notFound);
        }
        return fight[0];
    }
}

module.exports = new FightService();