const UserService = require('./userService');
const HTTPError = require('../helpers/httperror');
const statusCodes = require('../constants/statusCodes');

class AuthService {
    login(userData) {
        const user = UserService.search(userData);
        if(!user) {
            throw new HTTPError('User not found', statusCodes.notFound);
        }
        return user;
    }
}

module.exports = new AuthService();