const { Router } = require('express');
const FightController = require('../controllers/fightController');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFightValid, updateFightValid } = require('../middlewares/fight.validation.middleware');

const router = Router();

router.get('/', FightController.getAllController(), responseMiddleware);
router.get('/:id', FightController.getByIdController(), responseMiddleware);
router.post('/', createFightValid, FightController.postController(), responseMiddleware);
router.put('/:id', updateFightValid, FightController.updateController(), responseMiddleware);
router.delete('/:id', FightController.deleteController(), responseMiddleware);

module.exports = router;