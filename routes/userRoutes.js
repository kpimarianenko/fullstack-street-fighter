const { Router } = require('express');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');
const UserController = require('../controllers/userController');

const router = Router();

router.get('/', UserController.getAllController(), responseMiddleware);
router.get('/:id', UserController.getByIdController(), responseMiddleware);
router.post('/', createUserValid, UserController.postController(), responseMiddleware);
router.put('/:id', updateUserValid, UserController.updateController(), responseMiddleware);
router.delete('/:id', UserController.deleteController(), responseMiddleware);

module.exports = router;