const { Router } = require('express');
const AuthController = require('../controllers/authController');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post('/login', AuthController.loginController(), responseMiddleware);

module.exports = router;