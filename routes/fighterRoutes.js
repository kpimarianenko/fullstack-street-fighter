const { Router } = require('express');
const FighterController = require('../controllers/fighterController');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

router.get('/', FighterController.getAllController(), responseMiddleware);
router.get('/:id', FighterController.getByIdController(), responseMiddleware);
router.post('/', createFighterValid, FighterController.postController(), responseMiddleware);
router.put('/:id', updateFighterValid, FighterController.updateController(), responseMiddleware);
router.delete('/:id', FighterController.deleteController(), responseMiddleware);

module.exports = router;