const statusCodes = {
    ok: 200,
    notFound: 404,
    badRequest: 400,
    serverError: 500,
}

module.exports = statusCodes;