const regExps = {
    phoneNumber: /^\+380\d{9}$/,
    email: /^[\w.+\-]+@gmail\.com$/,
    uuid4: /^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/
}

module.exports = regExps;